﻿using Home6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Home6.Services
{
    public interface IStudentCollection
    {
        int Add(Student student);
        Student Get(int id);
        void Delete(int id);
        Student[] All();

    }
}
