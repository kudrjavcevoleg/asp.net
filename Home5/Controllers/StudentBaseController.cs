﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Home5.Models;

namespace Home5.Controllers
{
    public class StudentBaseController : Controller
    {
        public static List<Student> Base = new List<Student>();

        [HttpGet]
        public IActionResult Create()
        {
            return View("Edit", new Student());
        }

        [HttpPost]
        public IActionResult Create(Student student)
        {
            if (!ModelState.IsValid)
                return View("Error");
            Base.Add(student);
            return View("View", student);
        }

        [HttpGet]
        public IActionResult Read(int id)
        {
            if (id >= Base.Count)
                return View("Error");
            return View("View", Base[id]);
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            if (id >= Base.Count)
                return View("Error");
            return View("Edit", Base[id]);
        }

        [HttpPost]
        public IActionResult Update(int id, Student student)
        {
            if (!ModelState.IsValid)
                return View("Error");
            Base[id].Name = student.Name;
            Base[id].Age = student.Age;
            Base[id].Surname = student.Surname;
            return View("View", student);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            if(id >= Base.Count)
                return View("Error");
            Base.RemoveAt(id);
            return View("ViewAll", Base);
        }
        
        [HttpGet]
        public IActionResult ReadAll()
        {
            return View("ViewAll", Base);
        }
    }
}