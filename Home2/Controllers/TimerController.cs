﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace Home2.Controllers
{
    public class TimerController : Controller
    {
        private readonly RequestDelegate _next;

        public TimerController(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            DateTime time = DateTime.Now;
            await _next.Invoke(context);
            DateTime time1 = DateTime.Now;
            TimeSpan timer = time1 - time;
            Console.WriteLine(timer.ToString());
        }
    }
}