﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Home9.Models;
using Home9.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Home9.Controllers
{
    public class StudentBaseController : Controller
    {
        private IStudentCollection _service;

        public StudentBaseController(IStudentCollection service)
        {
            _service = service;
        } 

        [HttpGet]
        public IActionResult Create()
        {
           
            return View("Edit", new Student());
        }

        [HttpPost]
        public IActionResult Create([FromServices] IStudentCollection service, Student student)
        {
            if (!ModelState.IsValid)
                return View("Error");
            service.Add(student);
            return View("View", student);
        }

        [HttpGet]
        public IActionResult Read(int id)
        {
            var service = HttpContext.RequestServices.GetService<IStudentCollection>();
            return View("View", service.Get(id));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var service = ActivatorUtilities.CreateInstance<StudentCollection>(HttpContext.RequestServices);
            return View("Edit", service.Get(id));
        }

        [HttpPost]
        public IActionResult Update(int id, Student student)
        {
            if (!ModelState.IsValid)
                return View("Error");
            _service.Get(id).Name = student.Name;
            _service.Get(id).Age = student.Age;
            _service.Get(id).Surname = student.Surname;
            return View("View", student);
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            _service.Delete(id);
            return View("ViewAll");
        }
        
        [HttpGet]
        public IActionResult ReadAll()
        {
            return View("ViewAll");
        }
    }
}