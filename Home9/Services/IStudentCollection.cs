﻿using Home9.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Home9.Services
{
    public interface IStudentCollection
    {
        int Add(Student student);
        Student Get(int id);
        void Delete(int id);
        Student[] All();

    }
}
