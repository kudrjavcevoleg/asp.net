﻿using Home9.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Home9.Services
{
    public class StudentCollection : IStudentCollection
    {
        private Dictionary<int, Student> _collection = new Dictionary<int, Student>();

        private Random _random = new Random();

        public int Add(Student student)
        {
            var key = 0;
            while (_collection.ContainsKey(key))
                key = _random.Next();
            _collection.Add(key, student);
            return key;

        }

        public void Delete(int id)
        {
            _collection.Remove(id);

        }

        public Student[] All()
        {
            return _collection.Values.ToArray();
        }

        public Student Get(int id)
        {
            return _collection[id];
        }
    }
}
