﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Home3.Models;

namespace Home3.Controllers
{
    public class StudentBaseController : Controller
    {
        public static List<Student> Base = new List<Student>(); 

        public string Create(string name, string surname, int age)
        {
            Student student = new Student(name, surname, age);

            Base.Add(student);

            return "Created";
        }

        public Student Read(int id)
        {
            return Base[id];
        }

        public string Update(int id, string name, string surname, int age)
        {
            Base[id].Name = name;
            Base[id].Age = age;
            Base[id].Surname = surname;
            return "Updated";
        }

        public string Delete(int id)
        {
            Base.RemoveAt(id);
            return "Deleted";
        }
    }
}