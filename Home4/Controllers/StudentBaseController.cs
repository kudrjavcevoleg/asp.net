﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Home4.Models;

namespace Home4.Controllers
{
    public class StudentBaseController : Controller
    {
        public static List<Student> Base = new List<Student>();

        [HttpPost]
        public string Create(Student student)
        {
            if (!ModelState.IsValid)
                return "Bad data";
            Base.Add(student);
            return "Created";
        }

        [HttpGet]
        public Student Read(int id)
        {
            if (id >= Base.Count)
                return new Student();
            return Base[id];
        }

        [HttpPost]
        public string Update(int id, Student student)
        {
            if (!ModelState.IsValid)
                return "Bad data";
            Base[id].Name = student.Name;
            Base[id].Age = student.Age;
            Base[id].Surname = student.Surname;
            return "Updated";
        }

        [HttpDelete]
        public string Delete(int id)
        {
            if(id >= Base.Count)
                return "Bad data";
            Base.RemoveAt(id);
            return "Deleted";
        }
    }
}