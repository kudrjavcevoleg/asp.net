﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Home4.Models
{
    public class Student
    {
        [MaxLength(15)]
        public string Name { get; set; }
        [MaxLength(25)]
        public string Surname { get; set; }
        [Range(17, 55)]
        public int Age { get; set; }

        public Student(string name, string surname, int age)
        {
            Name = name;
            Surname = surname;
            Age = age;
        }

        public Student():this("","", 0)
        {
            
        }
    }
}
